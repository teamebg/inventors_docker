echo "server {
    listen 7070;
    listen [::]:7070;
    server_name $DOMAINs;
    location / {
        proxy_pass  http://127.0.0.1:7700;
    }
}" > /etc/nginx/sites-enabled/meilisearch

#!/bin/bash

MONGO=0
SQL=0

for i in "$@"; do
  case $i in
    --sql)
      SQL=1
      shift # past argument=value
      ;;
    --mongo)
      MONGO=1
      shift # past argument=value
      ;;
  esac
done

if [ $MONGO -gt 0 ]; then
    echo -e "Creating mongo \"inventors_node\" dump\n";
    rm -rf /var/www/inventors/api/public_html/public/storage/inventors_mongodump.gz
    mongodump --db=inventors_new --gzip --archive=/var/www/inventors/api/public_html/public/storage/inventors_mongodump.gz
    echo -e "\nDump saved in /var/www/inventors/api/public_html/public/storage/inventors_mongodump.gz\n";
fi

if [ $SQL -gt 0 ]; then
    echo -e "Creating MySQL \"inventors\" dump\n";
    rm -rf /var/www/inventors/api/public_html/public/storage/inventors.sql
    mysqldump inventors > /var/www/inventors/api/public_html/public/storage/inventors.sql
    echo -e "\nDump saved in /var/www/inventors/api/public_html/public/storage/inventors.sql\n";
fi
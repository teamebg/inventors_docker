#!/bin/bash
# Script to boot all required services
usermod -d /var/lib/mysql/ mysql \
    && service mysql start \
    && service apache2 start \
    && /etc/init.d/mongod start \
    && chown -R meilisearch:meilisearch /home/meilisearch/data.ms \
    && /etc/init.d/meilisearch start \
    && cd /var/www/inventors/node \
    && service nginx start
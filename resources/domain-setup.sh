#!/bin/bash

echo "<VirtualHost *:80>
    DocumentRoot \"/var/www/inventors/api/public_html\"
    ServerName $DOMAIN
    ServerAlias www.$DOMAIN

    <Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>" > /etc/apache2/sites-available/api.inventors.conf
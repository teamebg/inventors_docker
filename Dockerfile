# syntax=docker/dockerfile:1

# Ubuntu image to run Inventors project
FROM ubuntu:20.04
MAINTAINER kalashovluka@gmail.com

# Setting Up environment
ENV TZ=Europe/Astrakhan \
    DEBIAN_FRONTEND=noninteractive \
    DOMAIN=api.inventors.spinom.tk

# Installing base packages
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y curl wget nano screen git

# Setting Up Apache2
RUN apt-get install -y apache2
RUN a2enmod rewrite
COPY resources/domain-setup.sh /domain-setup.sh
RUN chmod 777 /domain-setup.sh && /domain-setup.sh && rm -f /domain-setup.sh
RUN a2ensite api.inventors.conf

# Setting Up MySQL
RUN apt-get install -y mysql-server

# Setting Up Node.js
RUN apt-get install -y npm
RUN npm install -g n yarn pm2
RUN n lts && hash -r

# Setting Up PHP
RUN apt-get install -y \
    php7.4 libapache2-mod-php7.4 php7.4-common \
    php7.4-mysql php7.4-gd php7.4-json php7.4-xml php7.4-mbstring \
    php7.4-readline php7.4-pdo php7.4-curl php7.4-zip

# Setting Up Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/local/bin/composer

# Setting Up MongoDB
RUN wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | apt-key add -
RUN echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-5.0.list
RUN apt-get update \
    && apt-get install -y \
    mongodb-org=5.0.6 mongodb-org-database=5.0.6 mongodb-org-server=5.0.6 \
    mongodb-org-shell=5.0.6 mongodb-org-mongos=5.0.6 mongodb-org-tools=5.0.6

COPY resources/mongod.init /etc/init.d/mongod
RUN chmod 777 /etc/init.d/mongod

# Setting Up Meilisearch
RUN useradd -m meilisearch || true
RUN groupadd meilisearch || true
RUN usermod -a -G meilisearch meilisearch || true
WORKDIR /usr/bin
RUN curl -L https://install.meilisearch.com | sh && chmod 777 ./meilisearch
COPY resources/meilisearch/init /etc/init.d/meilisearch
RUN chmod 777 /etc/init.d/meilisearch

# Providing Meilisearch reverse proxy
RUN apt-get install -y nginx
RUN rm -f /etc/nginx/sites-enabled/default
COPY resources/meilisearch/nginx-domain-setup.sh /domain-setup.sh
RUN chmod 777 /domain-setup.sh && /domain-setup.sh && rm -f /domain-setup.sh

# Setting Up SSH access to private repositories
RUN mkdir /root/.ssh/
COPY resources/ssh/id_rsa /root/.ssh/id_rsa
RUN chmod 400 /root/.ssh/id_rsa
COPY resources/ssh/id_rsa.pub /root/.ssh/id_rsa.pub
RUN chmod 400 /root/.ssh/id_rsa.pub
RUN touch /root/.ssh/known_hosts && ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

# Setting Up PHP repository
RUN mkdir -p /var/www/inventors/api/public_html
WORKDIR /var/www/inventors/api/public_html
RUN git clone git@bitbucket.org:teamebg/inventors_backend.git .

# Preparing persistent public storage for laravel
RUN rm -rf storage/app/public
RUN ln -s /root/storage/php-public-storage `pwd`/storage/app/public

RUN composer install
RUN chmod -R 777 storage 
RUN php artisan storage:link
RUN cp .env.example .env
RUN php artisan key:generate

# Setting Up auto deploy from master for PHP repository
RUN cp -r /root/.ssh /var/www
RUN chown -R www-data:www-data /var/www/.ssh
RUN chown -R www-data:www-data /var/www/inventors/api/public_html

# Setting Up PhpMyAdmin
RUN apt-get -y install phpmyadmin
RUN cp /etc/phpmyadmin/apache.conf /etc/apache2/conf-available/phpmyadmin.conf
RUN a2enconf phpmyadmin

# Setting Up SSL
RUN apt-get update --fix-missing && apt-get install -y certbot python3-certbot-apache
RUN certbot --apache --non-interactive --agree-tos --no-redirect -m bakhatdzegivi@gmail.com -d $DOMAIN || true

# Setting Up Node.js repository
RUN mkdir -p /var/www/inventors/node
WORKDIR /var/www/inventors/node
RUN git clone git@bitbucket.org:teamebg/inventors_node.git .
RUN yarn && yarn build || true

# Copy startup script
COPY resources/boot.sh /var/opt/boot.sh
RUN chmod 777 /var/opt/boot.sh

# Copy dump tool
COPY resources/mkdumps.sh /bin/mkdumps.sh
RUN chmod 777 /bin/mkdumps.sh

WORKDIR /var/www/inventors
CMD /bin/bash /var/opt/boot.sh && tail -f /dev/null
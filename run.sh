#!/bin/bash
docker run  \
    -d  \
    -i  \
    -t  \
    -p 80:80    \
    -p 443:443  \
    -p 7070:7070 \
    -v inventors-mysql:/var/lib/mysql   \
    -v inventors-mongodb:/var/lib/mongodb   \
    -v inventors-meilisearch:/home/meilisearch/data.ms \
    -v inventors-storage:/root/storage \
    --name inventors \
    inventors
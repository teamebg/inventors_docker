# TODO
Make multiple containers and use docker-compose

# Setup Steps
### Create volumes
- To create mysql volume, run `docker volume create inventors-mysql`
- To create mongodb volume, run `docker volume create inventors-mongodb`
- To create meilisearch volume, run `docker volume create inventors-meilisearch`
- Finally, to create shared storage volume, run `docker volume create inventors-storage`

### Build dockerfile
- From current directory, run `docker build -t inventors .`, which will create `inventors` image

### Run container
- Run `run.sh` script, which will initialize a ready-to-use container 
- ???
- PROFIT!

# Attach
The following command will create a new BASH session inside the container:
`docker container exec -it inventors /bin/bash`

# Dumps
To create database dumps, use `mkdumps` command
```
# Will create MySQL `inventors` database dump
mkdumps --sql

# Will create MongoDB `inventors_new` database dump
mkdumps --mongo

# Will create both
mkdumps --mongo --sql
```

# File transfer 
1. Find storage volume path by running `docker volume inspect inventors-storage`. Volume path will be written in `Mountpoint` property

### Transfer files into container
2. Put files into directory found in step 1
3. Enter docker container
4. Find your files in `/root/storage` directory

### Transfer files from container to host
2. Enter docker container
3. Put your files into `/root/storage` directory
4. Find your files in directory found in step 1